import { createApp } from 'vue';

import AntDesign from 'ant-design-vue';
import routes from './routes';
import App from './App.vue';

import 'ant-design-vue/dist/antd.css';

const app = createApp(App);

app.use(AntDesign);
app.use(routes);

app.mount('#app');