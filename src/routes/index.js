import Login from '../views/login';
import Sales from '../views/sales';

import { createRouter, createWebHashHistory } from 'vue-router';

const routes = [
  {
    path: '',
    name: 'login',
    component: Login
  },
  {
    path: '/sales',
    name: 'sales',
    component: Sales
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router;